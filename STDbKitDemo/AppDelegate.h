//
//  AppDelegate.h
//  STDbKitDemo
//
//  Created by yls on 13-12-13.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
